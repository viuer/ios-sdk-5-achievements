//  LTManager+Achievement.m
//  Created by Alexander Dovbnya on 3/23/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager+Achievement.h"
#import "LTManager+Private.h"

#import "LTData.h"

#import "LTRefreshAchievementSequenceOperation.h"
#import "LTAchievementSequenceOperation.h"

#import "LTMWLogging.h"

@implementation LTManager (Achievement)

- (void)achievementWithId:(NSString *)achievementId
                  success:(void (^)(LTAchievement *))success
                  failure:(void (^)(NSError *))failure {
    
    [self achievementsWithSuccess:^(NSArray *achievements) {
        [achievements enumerateObjectsUsingBlock:^(LTAchievement *achievement, NSUInteger idx, BOOL *stop) {
            if ([achievement.achievementId isEqualToString:achievementId]) {
                if (success) success(achievement);
                *stop = YES;
            }
        }];
    } failure:failure];
}

- (void)achievementsWithSuccess:(void (^)(NSArray *))success
                        failure:(void (^)(NSError *))failure {
    
    if (self.data.achievements) {
        if (success) success(self.data.achievements);
    } else {
        
        // define the success handler that will be used in both scenarions;
        void (^successHandler)(void) = nil;
        if (success) {
            __weak __typeof(self) weakSelf = self;
            successHandler = ^{
                success(weakSelf.data.achievements);
            };
        }
        
        // If not started yet just do start sequence because it'll fetch all achievements
        if (self.status > LTSDKStatusWaitingInitialization) {
            LTRefreshAchievementSequenceOperation *refreshOp = [LTRefreshAchievementSequenceOperation sequence];
            [refreshOp setSuccess:successHandler failure:failure];
            [self addStartPendingNetworkOperation:refreshOp];
            
        } else {
            [self addLazyStartSuccessBlock:successHandler];
            [self addLazyStartFailureBlock:failure];
            [self addStartOperation];
        }
    }
}


- (void)reportAchievementWithId:(NSString *)achievementId
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *))failure {
    if (self.status == LTSDKStatusFailed) {
        LTLogDebug(@"Lootsie's SDK is not initialized");
        return;
    }
    
    // Achievement sequence
    LTAchievementSequenceOperation *achievementOp = [LTAchievementSequenceOperation sequenceWithAchievementId:achievementId];
    [achievementOp setSuccess:success failure:failure];
    [self addStartPendingNetworkOperation:achievementOp];
}

@end
