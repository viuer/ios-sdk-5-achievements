//  LTManager+Achievement.h
//  Created by Alexander Dovbnya on 3/23/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"
#import "LTAchievement.h"

@interface LTManager (Achievement)

/*!
 * Method is called to get data about certain achievement
 * @param achivmentID ID of achievement to get data about
 * @param success Callback that returns achievement with specified ID
 * @param failure Callback about failure to receive specified achievement
 */
- (void)achievementWithId:(NSString *)achievementId
                  success:(void (^)(LTAchievement *achievement))success
                  failure:(void (^)(NSError *error))failure;

/*!
 * Method that is called to get rewards list
 * @param success Callback that returns achievements list
 * @param failure Callback about failure to get list
 */
- (void)achievementsWithSuccess:(void (^)(NSArray *achievements))success
                        failure:(void (^)(NSError *error))failure;

/*!
 * Method is called when user gets Achievement
 * @param achievementId Achievement ID that users gets
 * @param success Callback about successful achievement enrolment
 * @param failure Callback about unsuccessful achievemnt enrolment with the failure reason
 */
- (void)reportAchievementWithId:(NSString *)achievementId
                        success:(void (^)(void))success
                        failure:(void (^)(NSError *error))failure;

@end
