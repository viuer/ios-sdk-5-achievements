//  LTAchievementSequenceOperation.m
//  Created by Fabio Teles on 8/5/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTAchievementSequenceOperation.h"
#import "LTPostUserAchievementsOperation.h"
#import "LTBlockOperation.h"
#import "LTPostNotificationOperation.h"

#import "LTRecievedPointsData.h"
#import "LTAchievement.h"

#import "LTLootsie.h"
#import "LTData.h"

@implementation LTAchievementSequenceOperation

+ (instancetype)sequenceWithAchievementId:(NSString *)achievementId {
    // creates achievement operation
    LTPostUserAchievementsOperation *achievementOp = [LTPostUserAchievementsOperation POSTWithAchievementId:achievementId];
    
    __block NSDictionary *blockUserInfo;
    LTBlockOperation *afterAchievement = [[LTBlockOperation alloc] initWithBlock:^(LTOperationCompletionBlock completion) {
        [[LTManager sharedManager] achievementsWithSuccess:^(NSArray *achievements) {
            [achievements enumerateObjectsUsingBlock:^(LTAchievement *achievement, NSUInteger idx, BOOL *stop) {
                if ([achievement.achievementId isEqualToString:achievementId]) {
                    achievement.achieved = YES;
                    blockUserInfo = @{LTAchievementUserInfoKey: achievement};

                    *stop = YES;
                }
            }];
            completion(nil); // flag completion after updating block userInfo
        } failure:nil];
    }];
    
    [afterAchievement setSafeName:@"com.lootsie.manager.afterAchievement"];
    
    // post notification operation afterwards
    LTPostNotificationOperation *noteOp = [[LTPostNotificationOperation alloc] initWithNotificationName:LTAchievementReachedNotification object:[LTManager sharedManager] userInfoBlock:^NSDictionary *{
        return blockUserInfo;
    }];
#if ENABLE_ACCOUNT
    // Updates the user's info
    LTBlockOperation *updateDataOp = [LTBlockOperation autoSuccessOpWithBlock:^{
        [LTManager sharedManager].data.user.totalPoints = achievementOp.savedInfo.totalPoints;
    }];
    
    [updateDataOp setSafeName:@"com.lootsie.sequence.achievement.updateData"];
    return [self groupWithOperations:@[achievementOp, updateDataOp, afterAchievement, noteOp] maxConcurrentOperationCount:1];
#else
    return [self groupWithOperations:@[achievementOp, afterAchievement, noteOp] maxConcurrentOperationCount:1];
#endif
    
    
}

@end
