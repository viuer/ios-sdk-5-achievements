//  LootsieAchievement.m
//  Created by Fabio Teles on 7/7/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTAchievement.h"

@implementation LTAchievement

- (void)populateWithDictionary:(NSDictionary *)dictionary {
    self.achievementId  = [dictionary objectForKeyNilSafe:LTIdKey];
    self.name           = [dictionary objectForKeyNilSafe:LTNameKey];
    self.desc           = [dictionary objectForKeyNilSafe:LTDescriptionKey];
    self.points         = [[dictionary objectForKeyNilSafe:LTPointsKey] integerValue];
    self.achieved       = [[dictionary objectForKeyNilSafe:LTIsAchievedKey] boolValue];
    self.repeatable     = [[dictionary objectForKeyNilSafe:LTRepeatableKey] boolValue];
}

- (NSDictionary *)dictionary {
    return @{
             LTIdKey            : self.achievementId,
             LTNameKey          : self.name,
             LTDescriptionKey   : self.desc,
             LTPointsKey        : @(self.points),
             LTIsAchievedKey    : @(self.isAchieved),
             LTRepeatableKey    : @(self.isRepeatable)
             };
}

- (id)copyWithZone:(NSZone *)zone {
    return [[self class] entityWithDictionary:[self dictionary]];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"{\t\nName: %@\t\nDesc: %@\t\nRepeatable: %@\t\nPoints: %ld\nID=%@\n}",
            self.name, self.desc, (self.isRepeatable ? @"YES" : @"NO"), (long)self.points, self.achievementId];
}

@end