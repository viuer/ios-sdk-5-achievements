//  LTUserAchievementsOperation.m
//  Created by Alexander Dovbnya on 3/9/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTGetUserAchievementsOperation.h"
#import "LTNetworkManager.h"
#import "LTUserAchievements.h"
#import "LTAFURLResponseSerialization.h"

@interface LTGetUserAchievementsOperation ()

@property (nonatomic, readwrite, strong) LTUserAchievements *userAchievements;

@end

@implementation LTGetUserAchievementsOperation

+ (instancetype)GET {
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self authHeadersIncludingAppSecret:YES apiSession:NO userSession:YES];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"GET"
                                                                   path:LTAPIUserAchievementsPath
                                                             parameters:nil
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

- (NSError *)dataTaskDidFinishSuccessfully {
    self.userAchievements = [LTUserAchievements entityWithDictionary:(NSDictionary *)self.responseObject];
    return nil;
}

- (NSError *)formatDataTaskError:(NSError *)error {
    NSError *baseError = [super formatDataTaskError:error];
    return baseError;
}

@end
