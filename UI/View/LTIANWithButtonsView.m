//  LTIANWithButtonsView.m
//  Created by Alexander Dovbnya on 3/3/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTIANWithButtonsView.h"
#import "LTUIManager.h"


@interface LTIANWithButtonsView ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;
@property (nonatomic, weak) IBOutlet UILabel *pointsLabel;

@end

@implementation LTIANWithButtonsView

- (void)setupIANWithTitle:(NSString *)title detail:(NSString *)detail points:(NSInteger)points currencyName:(NSString *)currencyName textColor:(UIColor *)textColor {
    [super setupIANWithTitle:title
                      detail:detail
                      points:points
                currencyName:currencyName
                   textColor:textColor];
    [self setPoints:points textColor:textColor];
}

- (void)setPoints:(NSInteger)points textColor:(UIColor *)textColor {
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"+%ld ", (long)points]
                                                                                   attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:25],
                                                                                                NSForegroundColorAttributeName : textColor}];
    NSAttributedString *tempAttributedString = [[NSAttributedString alloc] initWithString:NSLocalizedStringFromTable(@"Lootsie points", @"LTLootsie", nil)
                                                                               attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15],
                                                                                            NSForegroundColorAttributeName : textColor}];
    [attrString appendAttributedString:tempAttributedString];
    self.pointsLabel.attributedText = attrString;
}



@end
