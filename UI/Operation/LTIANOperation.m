//  LTIANOperation.m
//  Created by Fabio Teles on 8/27/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTIANOperation.h"
#import "LTIANView.h"
#import "LTIANWithButtonsView.h"

#import "LTUIManager.h"
#import "LTAchievement.h"

CGFloat const LTIANHeight                       = 70;

@interface LTIANOperation ()

@end

@implementation LTIANOperation

- (instancetype)initWithAchievement:(LTAchievement *)achievement currencyName:(NSString *)currencyName textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor action:(void (^)(LTAchievement *))action presentationContextBlock:(UIViewController *(^)(void))presentationContextBlock {
    NSParameterAssert(achievement);
    
    if (self = [super initWithCurrencyName:currencyName textColor:textColor backgroundColor:backgroundColor action:^{
        if (action != nil) {
            action(achievement);
        }
    } presentationContextBlock:presentationContextBlock]) {
        _achievement = achievement;
    }
    return self;
}

- (LTIANWithButtonsView *)createIANView {
    NSBundle *bundle = [NSBundle bundleForClass:[LTUIManager class]];
    LTIANWithButtonsView *ianView = [[[UINib nibWithNibName:@"LTIANWithButtonsView" bundle:bundle] instantiateWithOwner:self options:nil] objectAtIndex:0];
    [ianView setupIANWithAchievement:self.achievement currencyName:self.currencyName textColor:self.textColor];
    [ianView.closeButton addTarget:self action:@selector(closeDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    [ianView.actionButton addTarget:self action:@selector(handleIANTap:) forControlEvents:UIControlEventTouchUpInside];
    [ianView.actionButton setTitle:NSLocalizedStringFromTable(@"My rewards >", @"LTLootsie", nil) forState:UIControlStateNormal];
    self.ianView = ianView;
    return ianView;
}

- (CGFloat)heightIANView {
    return 181.0f;
}

@end
