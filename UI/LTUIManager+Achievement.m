//  LTUIManager+Achievement.m
//  Created by Alexander Dovbnya on 3/24/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTUIManager+Achievement.h"
#if ENABLE_MARKETPLACE
#import "LTUIManager+Marketplace.h"
#endif

#import "LTIANOperation.h"

NSString * LTAchievementsSegueIdentifier	= @"AchievementsSegue";

@interface LTUIManager ()

@property (nonatomic, strong) UIViewController *wrapRootViewController;
@property (nonatomic, strong) UIViewController *presentationContext;

- (void)showPageWithSegueIdentifier:(NSString *)identifier;
- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController;

@end

@implementation LTUIManager (Achievement)

- (void)showAchievementsPage {
    [self showPageWithSegueIdentifier:LTAchievementsSegueIdentifier];
}

#pragma mark Notification

- (void)handleAchievementReached:(NSNotification *)notification {
    LTAchievement *achievement = [notification.userInfo objectForKey:LTAchievementUserInfoKey];
    if (achievement && self.ianBehavior != LTIANBehaviorDoNotAppear) {
        LTIANOperation *ianOp = [[LTIANOperation alloc] initWithAchievement:achievement
                                                               currencyName:[self.data.settings.currencyName uppercaseString]
                                                                  textColor:[self.data.app.textColor color]
                                                            backgroundColor:[self.data.app.backgroundColor color]
                                                                     action:[self actionForIAN]
                                                   presentationContextBlock:^UIViewController *{
                                                       return [self presentationContext];
                                                   }];
        [self->_generalQueue addOperation:ianOp];
    }
}

#pragma mark - IAN

- (void (^)(LTAchievement *))actionForIAN {
    if (self.ianBehavior == LTIANBehaviorDoNothing) return nil;
    
    __weak __typeof(self) weakSelf = self;
    return ^(LTAchievement *achievement){
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        
        [strongSelf sendUserActivityWithMetric:LTUserActivityMetricAchievementIANTapped value:@"1" activityId:nil success:nil failure:nil];
        
        switch (self.ianBehavior) {
            case LTIANBehaviorGoToRewards:
#if ENABLE_MARKETPLACE
                [strongSelf showRewardsPage];
#endif
                break;
            case LTIANBehaviorGoToAchievements:
                [strongSelf showAchievementsPage];
                break;
            case LTIANBehaviorGoToAbout:
                [strongSelf showAboutPage];
                break;
            case LTIANBehaviorGoToTOS:
                [strongSelf showTermsPage];
                break;
            default:
                break;
        }
    };
}

@end
