//  LTAchievementsPageController.m
//  Created by Fabio Teles on 5/20/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTAchievementsPageController.h"
#import "LTAchievementsCell.h"
#import "LTUIManager.h"
#import "LTManager+Achievement.h"
#import "LTTools.h"

#import "LTApp.h"
#import "LTData.h"
#import "LTImagePath.h"
#import "LTAchievement.h"
#import "LTSettings.h"

static NSString * AchievementCellIdentifier = @"AchievementCell";

@interface LTAchievementsPageController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *achievements;
@property (weak, nonatomic) IBOutlet UITableView *achievementsTableView;
@property (weak, nonatomic) IBOutlet UIImageView *appImageView;
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dynamicBottomConstraint;

@end

@implementation LTAchievementsPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.appNameLabel.text = [LTUIManager sharedManager].data.app.name;
    self.pointsLabel.text = @"";

	self.achievementsTableView.allowsSelection = NO;
}

- (void)loadAppImage {
    // async load image
    NSString *thumbnailUrl = [LTUIManager sharedManager].data.app.imagePaths.small;
    UIImageView *imageView = self.appImageView;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,  0ul);
    dispatch_async(queue, ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnailUrl]];
        dispatch_sync(dispatch_get_main_queue(), ^{
            imageView.image = [UIImage imageWithData:data];
        });
    });
}

- (void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];

	CGFloat constant = 0;
	CGFloat availableHeight = self.view.bounds.size.height;
	CGSize contentSize = [self.achievementsTableView contentSize];
	if (contentSize.height < availableHeight) {
		constant = availableHeight - contentSize.height;
	}
	self.dynamicBottomConstraint.constant = constant;

	[self.view layoutIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

    [self updateAchievements];
}

- (void)updateAchievements {
    __weak __typeof(self) weakSelf = self;
    [[LTUIManager sharedManager] achievementsWithSuccess:^(NSArray *fetchedAchievements) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        // separate into repeatables and non-repeatables
        NSMutableArray *repeatables = [NSMutableArray array];
        NSMutableArray *nonRepeatables = [NSMutableArray array];
        
        NSUInteger totalAchievementPoints = 0;
        
        for (LTAchievement *achievement in fetchedAchievements) {
            if (achievement.isRepeatable) {
                [repeatables addObject:achievement];
            } else {
                [nonRepeatables addObject:achievement];
            }
            totalAchievementPoints += achievement.points;
        }
        
        // filter before adding to final array
        NSMutableArray *result = [NSMutableArray array];
        if (nonRepeatables.count > 0) {
            [result addObject:nonRepeatables];
        }
        if (repeatables.count > 0) {
            [result addObject:repeatables];
        }
        strongSelf.achievements = [NSArray arrayWithArray:result];
        
        // reload table view
        strongSelf.pointsLabel.text = [NSString stringWithFormat:@"%lu %@", (long)totalAchievementPoints, [LTUIManager sharedManager].data.settings.currencyAbbreviation];
        [strongSelf.achievementsTableView reloadData];
        
        // load app image
        [strongSelf loadAppImage];
    } failure:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.achievements.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.achievements[section] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 28.f)];
	header.userInteractionEnabled = NO;
    header.backgroundColor = [LTTools colorWithHex:0xe9e9e9];

	UILabel *title = [UILabel new];
	UIFont *font;
	if ([UIFont respondsToSelector:@selector(systemFontOfSize:weight:)]) {
		font = [UIFont systemFontOfSize:14.f weight:UIFontWeightMedium];
	} else {
		font = [UIFont boldSystemFontOfSize:14.f];
	}
	title.font = font;
	title.textColor = [UIColor blackColor];

	if (section == 0) {
		title.text = NSLocalizedStringFromTable(@"One Time Achievements", @"LTLootsie", nil);
	} else {
		title.text = NSLocalizedStringFromTable(@"Repeatable Achievements", @"LTLootsie", nil);
	}

	title.translatesAutoresizingMaskIntoConstraints = NO;
	[header addSubview:title];

	[header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-25-[title]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(title)]];
	[header addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterY multiplier:1.f constant:0.f]];
	header.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);

	return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 28.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	LTAchievementsCell *cell = [tableView dequeueReusableCellWithIdentifier:AchievementCellIdentifier forIndexPath:indexPath];

	LTAchievement *achievement = self.achievements[indexPath.section][indexPath.row];

	cell.titleLabel.text = achievement.name;
	cell.descLabel.text = achievement.desc;
	cell.pointsLabel.text = [NSString stringWithFormat:@"%lu %@", (long)achievement.points, [LTUIManager sharedManager].data.settings.currencyAbbreviation];
    UIColor *achievementColor = achievement.isAchieved ? [LTTools colorWithHex:0xf85354] : [LTTools colorWithHex:0x9e9e9e];
    cell.checkmarkLabel.textColor = achievementColor;
    cell.checkmarkLabel.layer.borderColor = achievementColor.CGColor;
    cell.pointsLabel.textColor = achievementColor;
    cell.titleLabel.textColor = achievement.isAchieved ? [LTTools colorWithHex:0xf85354] : [UIColor blackColor];
	return cell;
}

@end
